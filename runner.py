import sys
import argparse
import http.server
import socketserver
import cgi
import json
import requests
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler
from urllib.parse import urlparse
from aiohttp import web
import asyncio

fh = TimedRotatingFileHandler("acs_integration.log", when='midnight')
sh = logging.StreamHandler()
logging.basicConfig(handlers=(fh, sh),  
                    format='[%(asctime)s.%(msecs)03d | %(levelname)s]: %(message)s', 
                    datefmt='%d.%m.%Y %H:%M:%S',
                    level=logging.INFO)

class ACS_Sigur:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.writer = None
        self.reader = None

    async def connect(self):
        try:
            logging.info("Trying connecting to the Sigur")
            self.reader, self.writer = await asyncio.open_connection(self.ip, self.port)
        except:
            logging.warning("Connection to the Sigur failed")
        else:
            logging.info("Connected to the Sigur")
        pass

    async def send_command(self, command, close_connection = False):
        self.writer.write(command.encode())
        try:
            logging.info(f"Message \'{command}\' is sending to Sigur")
            await self.writer.drain()
        except:
            logging.warning("Error on sending message to Sigur")
            self.writer.close()
            await writer.wait_closed()
            logging.warning("Connection closed")
            return False
        else:
            logging.info(f"Message sended to Sigur")
    
        try:
            logging.info("Reading answer from Sigur")
            data = await self.reader.readline()
        except:
            logging.warning("Error on reading from Sigur")
            self.writer.close()
            await writer.wait_closed()
            logging.warning("Connection closed")
        else:
            # data.replace('\n','')
            logging.info(f"Sigur answered: {data}")

        if "OK" in str(data):
            logging.info("Command executed successfull")
            if close_connection:
                self.writer.close()
                await self.writer.wait_closed()
                logging.info("Connection closed")
            return True
        else:
            logging.warning("Command failed. That is why:", data_reply)
            self.writer.close()
            await writer.wait_closed()
            logging.info("Connection closed")
            return False

def create_cmd_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--acs', action="store", dest="acs")
    parser.add_argument('-b', '--bind', action="store", dest="acs_url")    
    parser.add_argument('-l', '--listen', action="store", dest="listen_url")

    return parser

parser = create_cmd_parser()
args = parser.parse_args()
acs_url = urlparse(args.acs_url)
listen_url = urlparse(args.listen_url)

async def handler_http(request):
    host, port = request.transport.get_extra_info('peername')
    logging.info("New request from {host}:{port}".format(host = host, port = port))
    try:
        request_data = await request.json()
    except:
        logging.warning("There is empty request from a client")
        external_id = None
    else:
        logging.info("Request JSON is: {text}".format(text = request_data))
        try:
            external_id = request_data["data"]["person"]["external_id"]
        except:
            external_id = None
            logging.warning("There is no \'external_id\' in JSON")
    
    if args.acs == "Sigur":
        try:
            sigur = ACS_Sigur(acs_url.hostname, acs_url.port)
        except:
            return web.Response(text = "Connection failed", status = 400)
        
        try:
            command = "LOGIN 1.8 {username}{password}\r\n".format(\
                username = acs_url.username, password = " "+acs_url.password if acs_url.password != None else "") # Administrator
            await sigur.connect()
            is_login = await sigur.send_command(command = command, close_connection = False)
        except:
            return web.Response(text = "Error while Login", status = 400)
        else:
            if not is_login:
                return web.Response(text = "Login is failed", status = 403)
        
        logging.info(f"Trying to open a door for person_id = {external_id}")
        try:
            if external_id == None:
                command = "ALLOWPASS 1 ANONYMOUS IN\r\n"
            else:
                command = f"ALLOWPASS 1 {external_id} IN\r\n"
            await sigur.send_command(command = command, close_connection = True)
        except:
            logging.warning("Something went wrong")
            return web.Response(text = "Error while opening the door", status = 400)
        else:
            logging.info("Door opened")
        logging.info("Connection with the Sigur server closed")

    if args.acs == "Beward":
        logging.info("Trying connect to the Beward")
        uri = 'http://'+acs_url.hostname+'/cgi-bin/io/port.cgi?action=O0:/'
        session = aiohttp.ClientSession()
        try:
            async with aiohttp.ClientSession as session:
                async with session.get(url = uri, auth=(acs_url.username, acs_url.password)) as response:
                    if response.status_code == 200:
                        logging.info("Door was opened")
                    else:
                        logging.warning("Something went wrong")
        except:
            logging.warning("There was an error while sending command to Beward")
    return web.Response(text = "OK", status = 200)

app = web.Application()
app.add_routes([web.post('/',handler_http)])

def main():
    logging.info("Application started ACS - {ACS}, IP address - {ip}, port - {port}, serverport - {serverport}".format(ACS = args.acs, ip = acs_url.hostname, port = acs_url.port, serverport = listen_url.port))
    if args.acs == "Sigur":
        logging.info("ACS Sigur chosen")
    elif args.acs == "Beward":
        logging.info("ACS Beward chosen")
    else:
        logging.info("ACS is unknown. Please use \"Sigur\" or \"Beward\".\nIntegrated module shutted down.")
        sys.exit()

    logging.info("Starting web-server")
    web.run_app(app = app, port = listen_url.port)
    # try:
    #     with socketserver.TCPServer((listen_url.hostname, int(listen_url.port)), MyHandler) as httpd:
    #         logging.info("Started listening port {port}".format(port = listen_url.port))
    #         httpd.serve_forever()
    # except:
    #     logging.warning("Cannot connect to the listening port or someone shut application down")
    #     logging.exception("Massage")
    #     sys.exit()

if __name__ == "__main__":
    main()
